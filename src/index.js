require('dotenv').config()

const axios = require('axios')

const express = require('express')
const app = express()
const port = process.env.PORT

const { Webhook, MessageBuilder } = require('discord-webhook-node')

const homebridgeHook = axios.create({
  baseURL: process.env.HOMEBRIDGE_WEBHOOK
})

const discordHooks = {
  Meetings: new Webhook(process.env.DISCORD_WEBHOOK_MEETINGS),
  SmartHome: new Webhook(process.env.DISCORD_WEBHOOK_SMARTHOME)
}

const capitalize = str => `${str.charAt(0).toUpperCase()}${str.slice(1)}`

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/meeting/:person/:state', (req, res) => {
  const { person, state } = req.params
  // prettier-ignore
  const message =
    state === 'in'
      ? `It looks like ${capitalize(person)} is in a meeting. Hope you've got pants on.`
      : `Seems that ${capitalize(person)}'s meeting is over. Phew, glad that's done for...`
  discordHooks.Meetings.send(message)
  homebridgeHook.get('/', {
    params: {
      accessoryId: `${person}Meeting`,
      state: state === 'in'
    }
  })
  res.send({ message })
})

app.get('/doorbell/ring', (req, res) => {
  const imageURL = `https://nexusapi-us1.camera.home.nest.com/get_image?uuid=${
    process.env.NEST_CAMERA_UUID
  }&width=540&public=${process.env.NEST_CAMERA_ID}&t=${Date.now()}`
  const message = new MessageBuilder()
    .setTitle('Front Door Camera')
    .setDescription('Open Live Camera Feed')
    .setURL(`https://video.nest.com/live/${process.env.NEST_CAMERA_ID}`)
    .setImage(imageURL)
    .setTimestamp()

  discordHooks.SmartHome.send("🔔 Someone's at the front door")
  discordHooks.SmartHome.send(message)
  res.send({ success: true })
})

app.listen(port, () => {
  console.log(`Listening uwu at http://homebridge.gays:${port}`)
})
